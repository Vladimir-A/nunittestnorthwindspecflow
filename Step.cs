﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using TechTalk.SpecFlow;

namespace SpecflowTests.Steps
{
    [Binding]
    public class NWSteps
    {
        [Given(@"I open ""(.*)"" url")]
        public void GivenIOpenUrl(string url)
        {
            driver = new FirefoxDriver();
            driver.Url = url;
        }

        [When(@"I login with ""(.*)"" username and ""(.*)"" password")]
        public void WhenILoginWithUsernameAndPassword(string login, string password)
        {
            loginField = driver.FindElement(By.Id("Name"));
            passwordField = driver.FindElement(By.Id("Password"));

            loginField.SendKeys(login);
            passwordField.SendKeys(password);
            passwordField.Submit();
        }

        [Then(@"Login is successfull")]
        public void ThenLoginIsSuccessfull()
        {
            bool isLoginSuccessfull = false;
            try
            {
                IWebElement homePageLabel = driver.FindElement(By.XPath(".//*[text()='Home page']"));
                isLoginSuccessfull = homePageLabel.Displayed;
            }
            catch (NoSuchElementException e)
            {
                isLoginSuccessfull = false;
            }
            Assert.IsTrue(isLoginSuccessfull, "Login failed");
        }

        [Then(@"Login is failed")]
        public void ThenLoginIsFailed()
        {
            bool isLoginSuccessfull = false;
            try
            {
                IWebElement homePageLabel = driver.FindElement(By.XPath(".//*[text()='Home page']"));
                isLoginSuccessfull = homePageLabel.Displayed;
            }
            catch (NoSuchElementException e)
            {
                isLoginSuccessfull = false;
            }
            Assert.IsFalse(isLoginSuccessfull, "Login successfull for invalid credentials");
        }

        private IWebDriver driver;
        private IWebElement loginField;
        private IWebElement passwordField;
    }
}
